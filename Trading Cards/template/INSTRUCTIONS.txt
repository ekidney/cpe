# Trading Cards Generator

----------------------------------------------------------
For this to work, you need to install the [Next Generator Inkscape extension](https://gitlab.com/Moini/nextgenerator) from Maren Hachmann.

It is worth reading Máirín Duffy’s tutorials on this extension:

- How to automate graphics production with Inkscape // https://blog.linuxgrrl.com/2022/07/19/how-to-automate-graphics-production-with-inkscape/
- Part 2: How to automate graphics production with Inkscape // https://blog.linuxgrrl.com/2022/08/02/part-2-how-to-automate-graphics-production-with-inkscape/

----------------------------------------------------------
INSTRUCTIONS:

When you receive new data for a trading card, such as details about a new team member, follow these steps to update the CSV file using LibreOffice Calc:

    Fill in the new team member's details in the CSV file.

    Save the photo of the team member in the 'images' folder. In the spreadsheet, type the filename as it is, for example, "image.png."

    For the "FileName" heading in the CSV file, use the format FirstnameLastname (e.g., EmmaKidney).

    Save the CSV file and open the "Template.svg" file in Inkscape.

    Navigate to Extensions > Export > NextGenerator…

    Ensure the following options are filled in correctly:
        'CSV file:' should point to the path of your CSV file with the data (e.g., '../Trading Cards/template/trading-cards.csv').
        'Non-text values to replace:' should be set as '{"photo":"image.png"}'.
        'Number of sets in the template:' should be set to '1'.
        'File name pattern:' should be '%VAR_FileName%'.
        'Export file format:' should be 'PNG'.
        'DPI' should be set to '300'.
        'Save in:' should point to the location where you want the finalized cards to be saved (e.g., '../cpe/Trading Cards/template/Generated-Cards').

    Click on the "Apply" button.

Once you've applied these steps, the completed trading cards will be available in the designated save location.
